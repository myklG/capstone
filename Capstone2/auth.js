const jwt = require("jsonwebtoken");

const codeKey = "SadBoI2023";

// Creating access token

module.exports.createAccessToken = (user) => {
	const data = {
		userName : user.userName,
		id : user._id,
		isAdmin : user.isAdmin,
		email : user.email
	}

	return jwt.sign(data, codeKey, {});
}

module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;

	if(token !== undefined){
		token = token.slice(7, token.length)

			return jwt.verify(token, codeKey, (error, data) => {

				if(error){
					return response.send(false)

				}else{

					next();

				}
			})
	}else{
		return response.send(false)
	}

}
module.exports.decode = (token) => {

	token = token.slice(7, token.length);

	// The decode method is used to obtain the information from the JWT
	// The {complete: true} option allows us to return additional informatin from the JWT token


	return jwt.decode(token, {complete: true}).payload;
}