const Users = require(`../models/Users.js`);
const Products = require("../models/Products.js");

// encryting password
const bcrypt = require("bcrypt");

// requiring auth.js for authentication
const auth = require("../auth.js");


module.exports.userRegistration = (request, response) => {
	
	const userEmail = request.body.email;
	const userName = request.body.userName;

	Users.findOne({ $or: [{email : userEmail},{userName: userName}]})
	.then(result => {

		if(result){

			if(result.email === userEmail){
				response.send(false);
			}else if(result.userName === userName){
				response.send(false);
			}
		}else{

			let newUser = new Users({
				userName : userName,
				email : userEmail,
				password : bcrypt.hashSync(request.body.password, 10)

			});

			newUser.save()
			.then(save => response.send(true))
			.catch(error => response.send(false));
		}

	})
	.catch(error => response.send(false));

}

module.exports.loginUser = (request, response) => {

	const userEmail = request.body.email;

	Users.findOne({email : userEmail})
	.then(result => {
		if(result){

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return response.send({
					auth : auth.createAccessToken(result)
				});
			}else{
				return response.send(false)
			}

		}else{
			return response.send(false);
		}
	})
	.catch(error => response.send(false)); 
}


module.exports.createOrder = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const productId = request.body.id;


	if (userData.isAdmin) {
		return response.send(false);
	} else{

		let isUserUpdated = Users.findById(userData.id)
		.then(result => {

			Products.findById(productId)
			.then(product => {

				const quantity = request.body.quantity;
				const price = product.price;
				const total = price*quantity;

				result.orderedProduct.push({
				products : {
					productId :  productId,
					productName : product.name,
					quantity : quantity
				},
				totalAmount : total
			});

			result.save()
			.then(save => true)
			.catch(error => false);

			})

			

		})
		.catch(error => false);



		let isProductUpdated = Products.findById({_id : productId})
		.then(result => {

			Users.findById(userData.id)
			.then(user => {
				const orderID = user.orderedProduct[user.orderedProduct.length -1].id;

				result.userOrders.push({
					userId : userData.id,
					orderId : orderID

				});

				result.save()
				.then(save => true)
				.catch(error => false);
			})
		})
		.catch(error => false);

			if (isUserUpdated && isProductUpdated) {
				return response.send(true);
			}else{
				return response.send(false);
			}
		
	}

}

module.exports.getUserDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Users.findById(userData.id)
	.select(`_id userName email password isAdmin `)
	.then(result => {
			
			result.password = "*****"

			return response.send(result);
		
	})
	.catch(error => response.send(false));

}

module.exports.updateUser = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;

	let userUpdated = {
		isAdmin : request.body.isAdmin
	}

	if(userData.isAdmin) {
		
		Users.findByIdAndUpdate(userId, userUpdated)
		.then(result => {
			if(result.isAdmin === false && userUpdated.isAdmin === true){
				return response.send(`${result.userName} is now an admin`);
			}else if(result.isAdmin === true && userUpdated.isAdmin === false){
				return response.send(`${result.userName} is set as non-admin`);
			}
		})
		.catch(error => response.send(error));
	} else{
		return response.send("Your not an admin, you don't have access to this route!");
	}
}


module.exports.getOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false);
	}else{
		
		Users.findById(userData.id)
		.select(`userName orderedProduct -_id`)
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}

}
