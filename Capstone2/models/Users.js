const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	userName : {
		type : String,
		required : [true, `User Name is required!`]
	},

	email : {
		type : String,
		required : [true, `Email is required!`]
	},

	password : {
		type : String,
		required : [true, `Password is required!`]
	},

	isAdmin : {
		type : Boolean,
		default : false
	},

	orderedProduct : [
					{
							
	
								products : [
											{
												
													productId : {
														type: String,
														required : [true, `Product ID is required!`]
													},
	
													productName : {
														type : String,
														required : [true, `Product Name is required!`]
													},
	
													quantity : {
														type : Number,
														required : [true, `Quantity is required!`]
													}
											}
												
										   
								],
	
	
								totalAmount : {
									type : Number,
									required : [true, `Amount is required!`]
								},
	
								purchasedOn : {
									type : Date,
									default : new Date()
								}
	
	
							
						}

					 ]

});

const Users = new mongoose.model("Users", userSchema);

module.exports = Users;