const express = require(`express`);
const usersController = require("../controllers/usersControllers.js");

const auth = require("../auth.js");

const router = express.Router();

// Routes

// user registration routes
router.post("/register", usersController.userRegistration);

// user log-in
router.post("/login", usersController.loginUser);



// Route for userDetails
router.get("/userDetails", auth.verify, usersController.getUserDetails);

// Route for creating order
router.patch("/order", auth.verify, usersController.createOrder);

// ROute for setting user as Admin
router.patch("/:userId/setAdmin", auth.verify, usersController.updateUser);

// Route for getting authenticated users order
router.get("/getOrder", auth.verify, usersController.getOrders);



module.exports = router;