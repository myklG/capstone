const express = require("express");
const mongoose = require("mongoose");
const usersRoutes = require("./routes/usersRoutes.js");
const productsRoutes = require("./routes/productsRoutes.js")

const cors = require("cors");

const port = 6969;

const app = express();

// Stablish MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch288garcia.ltipebr.mongodb.net/SmartTechStoreAPI?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	});

let db = mongoose.connection;

db.on("error", console.error.bind(console, `Error, can't connect to the database!`));

db.once("open", () => console.log(`We are now connected to the database!`));


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// !!!!!!!!!!!!
app.use(cors());
// !!!!!!!!!!!!

// routings for the routes from usersRoutes
app.use("/users", usersRoutes);

// route for productsRoutes
app.use("/products", productsRoutes);





app.listen(port, () => console.log(`Server is running at port ${port}!`));