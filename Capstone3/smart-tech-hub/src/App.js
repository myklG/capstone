import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import AppNavBar from "./components/AppNavBar";
import Product from "./pages/Product";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import PageNotFound from "./pages/PageNotFound";

import AddProduct from "./pages/AddProduct";
import AllProduct from  "./pages/AllProduct";
import ProductView from "./pages/ProductView";
import UsersOrder from "./pages/UsersOrder";

import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";

import { useState, useEffect } from "react";

import { UserProvider } from "./UserContext";


function App() {
  const [user, setUser] = useState({ id: null, isAdmin: null });


  useEffect(() => {
    if(localStorage.getItem("token") === null) {
      setUser({

        id: null,
        isAdmin: null,
      })
      
    } else{

      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      })
        .then((result) => result.json())
        .then((data) => {
          console.log(data);
          setUser({
            userName : data.userName,
            id: data._id,
            isAdmin: data.isAdmin,
          });
        });

      

      }
  }, []);

  const unSetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    console.log(user);
    console.log(localStorage.getItem("token"));
  }, [user]);

  return (
    <UserProvider value={{ user, setUser, unSetUser }}>
      <BrowserRouter>
        <AppNavBar path="/product" element={<Product />} />
        <Routes>
          <Route path="/product" element={<Product />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path = '/logout' element={<Logout />} />
          <Route path = '/addProduct' element={<AddProduct />} />
          <Route path = '/allProduct' element={<AllProduct />} />
          <Route path = '/product/:productId' element={<ProductView/>} />
          <Route path = '/usersOrder' element={<UsersOrder/>} />
          <Route path="/notAccessible" element={<PageNotFound />} />
          <Route path="/*" element={<Navigate to="/notAccessible" />} />
          <Route path="/" element={<Navigate to="/product" />} />
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
