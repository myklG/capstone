import { Card, Button } from "react-bootstrap";

export default function AddProductCard(){

  const {description, name, price, isActive} = props.props;


    return(
      <Card>
      <Card.Body  className="d-flex flex-column justify-content-between">
      <div  >
      <Card.Subtitle>Status</Card.Subtitle>
      <Card.Text className="text-color">status</Card.Text>
      <Card.Subtitle>Price</Card.Subtitle>
      <Card.Text className="text-color">Php </Card.Text>
      <Card.Title >{name}</Card.Title>
      <Card.Subtitle className="mt-3">Description:</Card.Subtitle>
      <Card.Text className="mt-2">description</Card.Text>
      </div>
      <div className="text-center mt-2">
      <Button variant="primary">Update</Button>
      </div>
    </Card.Body>
  </Card>
    );
}