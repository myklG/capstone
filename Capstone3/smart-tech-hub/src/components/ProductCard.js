import {Card, Button} from "react-bootstrap";
import {  Link } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext } from "react";

export default function ProductCard(props) {


  
  const {user} = useContext(UserContext);

  const {_id, name, price, isActive, imgUrl} = props.productProps;
  
  const formattedPrice = price.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });



  return (

    user.isAdmin ? 
    <Card className="h-100">
     <Card.Img
      className="image-container mx-auto d-block"
      variant="top"
      src={imgUrl}
    />
    <Card.Body  className="d-flex flex-column justify-content-between">
      <div  >
      <Card.Subtitle>Status</Card.Subtitle>
      <Card.Text className={isActive ? 'text-color-active' : 'text-color-inactive'}>
  {isActive ? 'Active' : 'Deactivated'}
</Card.Text>

      <Card.Title >{name}</Card.Title>
      <Card.Subtitle>Price</Card.Subtitle>
      <Card.Text className="text-color">Php {formattedPrice}</Card.Text>
      </div>
      <div className="text-center mt-2">
      <Button  as ={Link}  to={`/product/${_id}`}  variant="primary">Update</Button>
      </div>
    </Card.Body>
  </Card>
  :
  <Card className="h-100">
    <Card.Img
      className="image-container mx-auto d-block"
      variant="top"
      src={imgUrl}
    />
    <Card.Body  className="d-flex flex-column justify-content-between">
      <div  >
      <Card.Title >{name}</Card.Title>
      <Card.Subtitle>Price</Card.Subtitle>
      <Card.Text className="text-color">Php {formattedPrice}</Card.Text>
      </div>
      <div className="text-center mt-2">
      <Button as ={Link} to={`/product/${_id}`}  variant="primary">View Details</Button>
      </div>
    </Card.Body>
  </Card>
  
  );
}
