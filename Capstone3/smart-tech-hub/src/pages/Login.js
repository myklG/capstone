import {
  Container,
  Row,
  Col,
  Button,
  Form
} from "react-bootstrap";

import { useState, useEffect, useContext } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isDisabled, setIsDisabled] = useState(true);

  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  function login(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "Application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        if (data === false) {
          Swal.fire({
            title: "User not found!",
            icon: "error",
            text: "You have entered wrong email or password, Please try again.",
          });
        } else {
          localStorage.setItem("token", data.auth);
          getUserDetails(data.auth);
          Swal.fire({
            title: `Logged in Successful!`,
            icon: "success",
          });
          navigate("/product");
        }
      });
  }

  const getUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((data) => {
        console.log(data);
        setUser({
          userName : data.userName,
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [user, password]);

  return(
    
    (user.id === null) ? 
    <Container className="page-bg">
      <Row>
        <Col className="col-sm-9 col-md-8  col-lg-6 mx-auto">
          <h1 className=" mar-top mb-3 title-text text-center ">Login</h1>
          <Form className="mt-3 decor p-3" onSubmit={(event) => login(event)}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className="bolder-text">Email address</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                placeholder="sample@mail.com"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label className="bolder-text">Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                placeholder="Password"
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <p>
                No account yet? <Link to={"/register"}>Sign up here</Link>
              </p>
            </Form.Group>

            <Form.Group className="d-flex justify-content-center">
              <Button variant="success" disabled={isDisabled} type="submit">
                Submit
              </Button>
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
   : 
    <Navigate to="/notAccessible" />
  );
}
