import {Navigate} from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';


export default function Logout(){
    
    const {unSetUser, setUser} = useContext(UserContext);

    useEffect(() => {

        unSetUser()

        setUser({
            id: null,
            isAdmin: null
          })
    })

    return(

        <Navigate to = '/product' />

    )
}