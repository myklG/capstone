import { useState, useEffect } from "react";
import ProductCard from "../components/ProductCard";
import { Container, Row, Col } from "react-bootstrap";

export default function Product() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
      .then(result => result.json())
      .then(data => {
        console.log(data)
        setProducts(data.map(products => {
          return(
            <Col sm={12} md={6} lg={4} key={products._id}  className="mb-4">
            <ProductCard productProps={products} />
          </Col>
          )
        }))
      });
  }, []);

  return (
    <>
    <h1 className="text-center mt-2 mb-3 title-text">Products</h1>
    <Container>
    <Row>{products}</Row>
  </Container>
  </>
  );
}
