import {Container, Row, Col, Card, Button} from "react-bootstrap";

import {useState, useEffect} from "react";
import UserContext from "../UserContext";
import { useContext } from "react";

import Swal from "sweetalert2";

import {useParams, Link} from 'react-router-dom';

export default function ProductView(props){

    const [orderCount, setorderCount] = useState(0);
    

    const [imgUrl, setImgUrl] = useState('');
    const [isActive, setIsActive] = useState('');
    const [price, setPrice] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');

    const [isDisable, setIsDisable] = useState(false);

    const {user} = useContext(UserContext);

    const {productId} = useParams();



    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(result => result.json())
        .then(data => {
            setIsActive(data.isActive);
            setName(data.name);
            setPrice(data.price.toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 }));
            setDescription(data.description);
            setImgUrl(data.imgUrl);
        });
    }, [])
      
const update = (productId) => {
    
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/updateProduct`,{
            method : 'PATCH',
            headers: {
                Authorization : `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'Application/json'
            },
            body: JSON.stringify({
                imgUrl: imgUrl,
                name: name,
                price: price,
                description: description
            })
        })
        .then(result => result.json())
        .then(data => {
			if(data){
				Swal.fire({
					title: 'Update Successful',
					icon : 'success',
					text: `${name} has been updated!`
				})
                .then(() => {
                    window.location.reload();
                  });

			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})	
			}
        })

      }

      const activate = (productId) => {
    
        
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activateProduct`,{
                method : 'PATCH',
                headers: {
                    Authorization : `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type' : 'Application/json'
                },
                body: JSON.stringify({
                    isActive : true
                })
            })
            .then(result => result.json())
            .then(data => {
                if(data){
                    Swal.fire({
                        title: 'Update Successful',
                        icon : 'success',
                        text: `${name} has been updated!`
                    })
                    .then(() => {
                        window.location.reload();
                      });
    
                }else{
                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try againProduct is already activaed.'
                    })	
                }
            })
    
          }

          const deactivate = (productId) => {
    
        
            fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archiveProduct`,{
                method : 'PATCH',
                headers: {
                    Authorization : `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type' : 'Application/json'
                },
                body: JSON.stringify({
                    isActive : false
                })
            })
            .then(result => result.json())
            .then(data => {
                if(data){
                    Swal.fire({
                        title: 'Update Successful',
                        icon : 'success',
                        text: `${name} has been updated!`
                        
                    })
                    .then(() => {
                        window.location.reload();
                      });
    
                }else{
                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try againProduct is already deactive.'
                    })	
                }
            })

           
    
          }

          const orderProduct = () => {
    
            fetch(`${process.env.REACT_APP_API_URL}/users/order`,{
                method : 'PATCH',
                headers: {
                    Authorization : `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type' : 'Application/json'
                },
                body: JSON.stringify({
                    id: productId,
                    quantity : orderCount
                })
            })
            .then(result => result.json())
            .then(data => {
                if(data){
                    Swal.fire({
                        title: 'Order Successful',
                        icon : 'success',
                        text: `You have purchased ${name}!`
                    })
                    .then(() => {
                        window.location.reload();
                      });
    
                }else{
                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.'
                    })	
                }
            })
    
          }

    function  add(subtract) {
        setorderCount(orderCount +1)

       
    }
    function subtract(){
       
        if (orderCount > 0) {
            setorderCount(orderCount -1);
          }
        
    }

    useEffect(() => {
        if(orderCount === 0){
            setIsDisable(true);
        }else{
            setIsDisable(false)
        }
    }, [orderCount])
    

    return(

        user.isAdmin ? 

        <Container className="mt-3">
            <Row>
                <Col className="col-12">        
                <h1 className="mb-3 title-text text-center">Product Details</h1>   
                    <Card className="h-100">
                        <Card.Img
                        className="img mx-auto d-block"
                        variant="top"
                        src={imgUrl}
                        />
                        <Card.Body  className="d-flex flex-column justify-content-between">
                        <div  >
                        <Card.Subtitle>Status</Card.Subtitle>
                        <Card.Text className={isActive ? 'text-color-active' : 'text-color-inactive'}>
                        {isActive ? 'Active' : 'Deactivated'}
                        </Card.Text>
                        <Card.Subtitle className="mt-3">Img URL:</Card.Subtitle>
                        <Card.Text className="mt-2">
                        <textarea
                                className="form-control"
                                value={imgUrl}
                                onChange={(event) => setImgUrl(event.target.value)}
                                rows={1}
                                />
                        </Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text className="text-color">Php 
                        <textarea
                                className="form-control"
                                value={price}
                                onChange={(event) => setPrice(event.target.value)}
                                rows={1}
                                />
                        </Card.Text>
                        <Card.Title ></Card.Title>
                        <Card.Subtitle className="mt-3">Name:</Card.Subtitle>
                        <Card.Text className="mt-2">
                        <textarea
                                className="form-control"
                                value={name}
                                onChange={(event) => setName(event.target.value)}
                                rows={1}
                                />
                        </Card.Text>
                        <Card.Subtitle className="mt-3">Description:</Card.Subtitle>
                        <Card.Text className="mt-2">
                        <textarea
                                className="form-control"
                                value={description}
                                onChange={(event) => setDescription(event.target.value)}
                                rows={3}
                                />
                        </Card.Text>
                        </div>
                        <div className="text-center mt-4">
                        <Button className="btn-size" onClick = {() => update(productId)}  variant="primary"  >Update</Button>
                        <Button onClick = {() => activate(productId)}  className="btn-size me-3 ms-3"  variant="primary bg-success" >Activate</Button>
                        <Button onClick = {() => deactivate(productId) }  className="btn-size bg-danger"  variant="primary">Deactivate</Button>
                        </div>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        :
        <>
            
        <Container className="mt-3">
        <h1 className="mt-2 mb-3 title-text text-center">Product Details</h1>
            <Row>
                <Col className="col-12">           
                    <Card className="h-100">
                        <Card.Img
                        className="img mx-auto d-block"
                        variant="top"
                        src={imgUrl}
                        />
                        <Card.Body  className="d-flex flex-column justify-content-between">
                        <div  >
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text className="text-color">Php {price}</Card.Text>
                        <Card.Title ></Card.Title>
                        <Card.Subtitle className="mt-3">Name:</Card.Subtitle>
                        <Card.Text className="mt-2">{name}</Card.Text>
                        <Card.Subtitle className="mt-3">Description:</Card.Subtitle>
                        <Card.Text className="mt-2">{description}</Card.Text>
                        </div>
                        { user.id === null ? (
                            <div className="text-center mt-2">
                        <Button as={Link} to="/login" variant="primary">Login to Order</Button>
                        </div>
                        ) : (
                        
                        <div className="text-center mt-2 ">
                        <div className="inline-container mx-auto  mb-3">
                        <button className="small-button" disabled={isDisable} onClick={subtract}>-</button>
                        <Card.Text className="mt-2 border text-box">{orderCount}</Card.Text>
                        <button className="small-button"  onClick={add}>+</button>
                        </div>
                        <Button onClick={orderProduct} variant="primary">Order</Button>
                        </div>
                        )}
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
        </>
    )

}