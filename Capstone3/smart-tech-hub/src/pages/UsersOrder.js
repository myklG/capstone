import { Container, Row, Col } from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import { useState, useEffect } from "react";
import UserContext from "../UserContext";
import { useContext } from "react";

export default function UsersOrder() {

const [userOrder, setUserOrder] = useState([]);

const {user} = useContext(UserContext);

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/getOrder`,{
        method: 'GET',
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
    })
    .then(result => result.json())
    .then(data => {
      
      setUserOrder(data.orderedProduct);
    });
}, []);
console.log(user);

const formatDate = (dateString) => {
  const date = new Date(dateString);
  const formattedDate = `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
  return formattedDate;
};

return user.id !== null && user.isAdmin === false ? (
  <Container className='page-bg'>
    <Row>
    <h1 className='text-center'>Order History</h1>
      <Col className='col-10 mx-auto'>
        <div className="table-responsive">
          <table className="table table-bordered text-center">
            <thead>
              <tr>
                <th style={{ backgroundColor: 'rgb(184, 146, 106)', color: 'white' }}>Product Name</th>
                <th style={{ backgroundColor: 'rgb(184, 146, 106)', color: 'white' }} >Date Purchased</th>
                <th style={{ backgroundColor: 'rgb(184, 146, 106)', color: 'white' }}>Quantity</th>
                <th style={{ backgroundColor: 'rgb(184, 146, 106)', color: 'white' }}>Total</th>
              </tr>
            </thead>
            <tbody>
              {userOrder.map((order) =>
                order.products.map((product) => (
                  <tr key={product._id}>
                    <td>{product.productName}</td>
                    <td>{formatDate(order.purchasedOn)}</td>
                    <td>{product.quantity}</td>
                    <td style={{ color: 'rgb(228, 121, 82)' }}>
                      {order.totalAmount && typeof order.totalAmount === "number"
                        ? order.totalAmount.toLocaleString("en-PH", {
                            style: "currency",
                            currency: "PHP",
                          })
                        : "N/A"}
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        </div>
      </Col>
    </Row>
  </Container>
) : (
  <Navigate to="/*" />
);
}